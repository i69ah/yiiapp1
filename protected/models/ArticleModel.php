<?php


class ArticleModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'tbl_articles';
    }

    public function __get($name)
    {
        if ($name === 'author') {
            $author = (new AuthorModel)->findByAttributes(['authorId' => (int)$this->getAttribute('authorId')]);
            return $author->getAttribute('name');
        }
        return parent::__get($name);
    }
}