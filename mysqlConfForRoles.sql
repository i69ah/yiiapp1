drop table if exists testdrive.AuthAssignment;
drop table if exists testdrive.AuthItemChild;
drop table if exists testdrive.AuthItem;

create table testdrive.AuthItem
(
    name        varchar(64) not null,
    type        integer     not null,
    description text,
    bizrule     text,
    data        text,
    primary key (name)
) engine InnoDB;

create table testdrive.AuthItemChild
(
    parent varchar(64) not null,
    child  varchar(64) not null,
    primary key (parent, child),
    foreign key (parent) references testdrive.AuthItem (name) on delete cascade on update cascade,
    foreign key (child) references testdrive.AuthItem (name) on delete cascade on update cascade
) engine InnoDB;

create table testdrive.AuthAssignment
(
    itemname varchar(64) not null,
    userid   varchar(64) not null,
    bizrule  text,
    data     text,
    primary key (itemname, userid),
    foreign key (itemname) references testdrive.AuthItem (name) on delete cascade on update cascade
) engine InnoDB;
