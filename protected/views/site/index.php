<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
?>

    <h1>
        <?= ucfirst(Yii::app()->user->name) ?>, welcome to
        <strong><?php echo CHtml::encode(Yii::app()->name); ?></strong>
    </h1>
<?php
if (Yii::app()->user->isGuest) {
    ?>
    <h2>Here are our authors</h2>
    <div class="users">
        <?php
        foreach ($authors->findAll() as $author) {
            ?>
            <div class="user">
                <div class="username">
                    <?= $author->name ?>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
    <?php
}
if (Yii::app()->user->checkAccess('admin')) {
    ?>
    <div class="users">
        <?php
        foreach ($users->findAll() as $user) {
            ?>
            <div class="user">
                <div class="username">
                    <?= $user->username ?>
                </div>
                <div class="password">
                    <?= $user->password ?>
                </div>
                <div class="email">
                    <?= $user->email ?>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
    <?php
}
?>