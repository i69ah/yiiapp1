<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
?>

<h1><?= ucfirst(Yii::app()->user->name) ?>, these are all articles you can see</strong></h1>
<div class="articles">
    <?php
    foreach ($articles->findAll() as $article) {
        ?>
        <div class="<?= $article->author === Yii::app()->user->name ? 'article-nohover' : 'article' ?>">
            <div class="article__title">
                <?= $article->title ?>
            </div>
            <div class="article__content">
                <?= $article->content ?>
            </div>
            <div class="hr"></div>
            <div class="article__author">
                <?= $article->author ?>
            </div>
            <?php
            $params = ['authorName' => $article->author];
            if (Yii::app()->user->checkAccess('updateArticle', $params)) {
                ?>
                <form action="/index.php?r=site/editarticle&id=<?= $article->id ?>" method="post">
                    <button type="submit">Edit</button>
                </form>
                <?php
            }
            ?>
        </div>
        <?php
    }
    ?>
</div>