<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
?>


<?php
//var_dump(Yii::app()->user->name === $author->name);die;
$params = ["authorName" => $author->name];
if (!Yii::app()->user->checkAccess('updateArticle', $params)) {
    ?>
    <h1><?= ucfirst(Yii::app()->user->name) ?>, sorry, you have no access to this page</strong></h1>
    <?php
} else if (Yii::app()->user->checkAccess('updateArticle', $params)) {
    ?>
    <h1><?= ucfirst(Yii::app()->user->name) ?>, let's change something</strong></h1>
    <div class="articles">
        <form action="/index.php?r=site/adminupdatearticle&id=<?= $article->id ?>" method="post">
            <div class="article__new upload__article">
                <input type="text" name="title" placeholder="<?= $article->title ?>">
                <textarea name="content" placeholder="<?= $article->content ?>"></textarea>
                <div class="sub">
                    <button type="submit">Submit</button>
                </div>
            </div>
        </form>
    </div>
    <?php
}
?>