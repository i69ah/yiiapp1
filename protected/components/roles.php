<?php

$auth = Yii::app()->authManager;

$bizRule = 'return Yii::app()->user->name === $params["authorName"];';
$auth->createOperation('updateArticle', 'edit article', $bizRule);

$bizRule = 'in_array($params["authorsNames"], Yii::app()->user->name);';
$auth->createOperation('createArticle', 'create article', $bizRule);

$author = $auth->createRole('author');
$author->addChild('createArticle');
$author->addChild('updateArticle');

$bizRule = 'return Yii::app()->user->name === "admin";';
$admin = $auth->createRole('admin', 'администратор', $bizRule);
$admin->addChild('author');

$bizRule = 'return Yii::app()->user->isGuest;';
$guest = $auth->createRole('guest', 'гость', $bizRule);

$bizRule = 'return !Yii::app()->user->isGuest;';
$user = $auth->createRole('commonUser', 'обычный пользователь', $bizRule);
$user->addChild('author');

$auth->assign('admin', 22);
for ($i = 1; $i < 22; $i++) {
    $auth->assign('commonUser', $i);
}

//$auth->assign('admin', 3);
//$auth->assign('commonUser', 1);
//$auth->assign('commonUser', 2);
