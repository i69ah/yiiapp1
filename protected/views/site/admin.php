<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
?>


<?php
if (!Yii::app()->user->checkAccess('admin')) {
    ?>
    <h1><?= ucfirst(Yii::app()->user->name) ?>, sorry, you have no access to this page</strong></h1>
    <?php
} else if (Yii::app()->user->checkAccess('admin')) {
    ?>
    <h1><?= ucfirst(Yii::app()->user->name) ?>, let's change something</strong></h1>
    <div class="articles">
        <form action="/index.php?r=site/uploadarticle" method="post">
            <div class="article__new upload__article">
                <input type="text" name="title" placeholder="Enter the title of your article">
                <textarea name="content" placeholder="Enter the content of your article"></textarea>
                <div class="sub">
                    <button type="submit">Submit</button>
                </div>
            </div>
        </form>
        <?php
        foreach ($articles->findAll() as $article) {
            ?>
            <form action="/index.php?r=site/adminupdatearticle&id=<?= $article->id ?>" method="post">
                <div class="article__new">
                    <input type="text" name="title" placeholder="<?= $article->title ?>">
                    <textarea name="content" placeholder="<?= $article->content ?>"></textarea>
                    <div class="hr"></div>
                    <div class="article__author">
                        <?= $article->author ?>
                    </div>
                    <div class="sub">
                        <button type="submit">Submit</button>
                    </div>
                </div>
            </form>
            <?php
        }
        ?>
    </div>
    <?php
}
?>